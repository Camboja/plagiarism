package com.plagiarism.plagiarism.uploadfile;

import com.plagiarism.plagiarism.detector.utility.Serializer;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class UploadFileService {
    private final UploadFileRepository uploadFileRepository;

    public UploadFileService(UploadFileRepository uploadFileRepository) {
        this.uploadFileRepository = uploadFileRepository;
    }

    public UploadFile downloadFile(long id) {
        return uploadFileRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public UploadFile uploadFile(MultipartFile file) throws IOException {
        UploadFile uploadFile = new UploadFile(file.getOriginalFilename(), file.getBytes(),
                Serializer.serializeToTokenByteArr(file.getBytes()), file.getContentType());
        return uploadFileRepository.save(uploadFile);
    }
}
