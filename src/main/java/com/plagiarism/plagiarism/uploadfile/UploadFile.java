package com.plagiarism.plagiarism.uploadfile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;

@Entity
@Table(name = "upload_file")
public class UploadFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_id")
    private long id;
    @Column(name = "file_name")
    private String filename;
    @Column(name = "file_data")
    @Lob
    private byte[] fileData;
    @Column(name = "file_tokenize_data")
    @JsonIgnore
    @Lob
    private byte[] fileTokenizeData;

    @Column(name = "content_type")
    private String contentType;

    public UploadFile() {
    }

    public UploadFile(String filename, byte[] fileData, byte[] fileTokenizeData, String contentType) {
        this.filename = filename;
        this.fileData = fileData;
        this.fileTokenizeData = fileTokenizeData;
        this.contentType = contentType;
    }

    public long getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String fileName) {
        this.filename = fileName;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }

    public byte[] getFileTokenizeData() {
        return fileTokenizeData;
    }

    public void setFileTokenizeData(byte[] fileTokenizeData) {
        this.fileTokenizeData = fileTokenizeData;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
