package com.plagiarism.plagiarism.uploadfile;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class UploadFileController {
    private final UploadFileService uploadFileService;

    public UploadFileController(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    @GetMapping("/upload/{id}")
    public ResponseEntity<UploadFile> downloadFile(@PathVariable("id") long id) {
        UploadFile file = uploadFileService.downloadFile(id);
        return ResponseEntity.ok(file);
    }

    @CrossOrigin
    @PostMapping("/upload")
    public ResponseEntity<UploadFile> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        return ResponseEntity.ok(uploadFileService.uploadFile(file));
    }
}
