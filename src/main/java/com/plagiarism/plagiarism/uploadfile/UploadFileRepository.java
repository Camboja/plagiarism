package com.plagiarism.plagiarism.uploadfile;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UploadFileRepository extends JpaRepository<UploadFile, Long> {
    List<TokenizeFile> findAllByIdNot(long id);
}
