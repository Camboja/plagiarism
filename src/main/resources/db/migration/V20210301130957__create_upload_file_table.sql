CREATE TABLE IF NOT EXISTS upload_file
(
    file_id   serial PRIMARY KEY,
    file_name varchar(255),
    file_data bytea,
    file_tokenize_data bytea,
    content_type varchar(255)
)
